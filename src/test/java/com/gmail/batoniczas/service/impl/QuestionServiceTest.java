package com.gmail.batoniczas.service.impl;

import com.gmail.batoniczas.entity.Advancement;
import com.gmail.batoniczas.entity.Answer;
import com.gmail.batoniczas.entity.Exam;
import com.gmail.batoniczas.entity.Question;
import com.gmail.batoniczas.service.IQuestionConnector;
import com.gmail.batoniczas.service.IQuestionService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class QuestionServiceTest {
    @Test
    void createEmptyListQuestion() {
        IQuestionConnector iQuestionConnector = mock(IQuestionConnector.class);
        QuestionService questionService = new QuestionService(iQuestionConnector);
        when(iQuestionConnector.getJsonArray()).thenReturn(new JSONArray());
        List<Question> listQuestion = questionService.createListQuestion();
        assertThat(listQuestion).isEmpty();
    }

    @Test
    void createSingletonListQuestion() {
        IQuestionConnector iQuestionConnector = mock(IQuestionConnector.class);
        QuestionService questionService = new QuestionService(iQuestionConnector);
        JSONArray put = new JSONArray().put(new JSONObject()
                .put("id", 1)
                .put("advancement", "medium")
                .put("category", "abc")
                .put("text", "text")
                .put("answers", new JSONArray()
                        .put(new JSONObject()
                                .put("text", "tekst")
                                .put("correct", true)
                                .put("explanation", "text"))));
        when(iQuestionConnector.getJsonArray()).thenReturn(put);
        List<Question> listQuestion = questionService.createListQuestion();
        assertThat(listQuestion).contains(new Question(1, Advancement.MEDIUM, "abc", "text", null, List.of(new Answer("tekst", true, "text"))));
    }

    @Test
    void createDoubleListQuestion() {
        IQuestionConnector iQuestionConnector = mock(IQuestionConnector.class);
        QuestionService questionService = new QuestionService(iQuestionConnector);
        JSONArray put = new JSONArray().put(new JSONObject()
                .put("id", 1)
                .put("advancement", "medium")
                .put("category", "abc")
                .put("text", "text")
                .put("answers", new JSONArray()
                        .put(new JSONObject()
                                .put("text", "tekst")
                                .put("correct", true)
                                .put("explanation", "text"))
                        .put(new JSONObject()
                                .put("text", "tekst")
                                .put("correct", false)
                                .put("explanation", "text"))));
        when(iQuestionConnector.getJsonArray()).thenReturn(put);
        List<Question> listQuestion = questionService.createListQuestion();
        assertThat(listQuestion).containsExactly(new Question(1, Advancement.MEDIUM, "abc", "text", null,
                List.of(new Answer("tekst", true, "text"), new Answer("tekst", false, "text"))));
    }

    @Test
    void createSomeListQuestion() {
        IQuestionConnector iQuestionConnector = mock(IQuestionConnector.class);
        QuestionService questionService = new QuestionService(iQuestionConnector);
        JSONArray put = new JSONArray().put(new JSONObject()
                        .put("id", 1)
                        .put("advancement", "medium")
                        .put("category", "abc")
                        .put("text", "text")
                        .put("answers", new JSONArray()
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", true)
                                        .put("explanation", "text"))
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", false)
                                        .put("explanation", "text"))))
                .put(new JSONObject()
                        .put("id", 2)
                        .put("advancement", "medium")
                        .put("category", "abc")
                        .put("text", "text")
                        .put("answers", new JSONArray()
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", true)
                                        .put("explanation", "text"))
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", false)
                                        .put("explanation", "text"))));
        when(iQuestionConnector.getJsonArray()).thenReturn(put);
        List<Question> listQuestion = questionService.createListQuestion();
        assertThat(listQuestion).containsExactlyInAnyOrder(new Question(1, Advancement.MEDIUM, "abc", "text", null,
                        List.of(new Answer("tekst", true, "text"), new Answer("tekst", false, "text"))),
                new Question(2, Advancement.MEDIUM, "abc", "text", null,
                        List.of(new Answer("tekst", true, "text"), new Answer("tekst", false, "text"))));
    }

    @Test
    void questionByAdvancement() {
        // given
        IQuestionConnector iQuestionConnector = mock(IQuestionConnector.class);
        IQuestionService iQuestionService = new QuestionService(iQuestionConnector);
        JSONArray put = new JSONArray().put(new JSONObject()
                        .put("id", 1)
                        .put("advancement", "basic")
                        .put("category", "abc")
                        .put("text", "text")
                        .put("answers", new JSONArray()
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", true)
                                        .put("explanation", "text"))
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", false)
                                        .put("explanation", "text"))))
                .put(new JSONObject()
                        .put("id", 2)
                        .put("advancement", "medium")
                        .put("category", "abc")
                        .put("text", "text")
                        .put("answers", new JSONArray()
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", true)
                                        .put("explanation", "text"))
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", false)
                                        .put("explanation", "text"))))
                .put(new JSONObject()
                        .put("id", 3)
                        .put("advancement", "expert")
                        .put("category", "abc")
                        .put("text", "text")
                        .put("answers", new JSONArray()
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", true)
                                        .put("explanation", "text"))
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", false)
                                        .put("explanation", "text"))));
        when(iQuestionConnector.getJsonArray()).thenReturn(put);
        // when
        Map<Advancement, List<Question>> advancementListMap = iQuestionService.questionByAdvancement();
        // then
        assertThat(advancementListMap).contains(
                Map.entry(Advancement.BASIC, List.of(
                        new Question(1, Advancement.BASIC, "abc", "text", null, List.of(
                                new Answer("tekst", true, "text"),
                                new Answer("tekst", false, "text"))))),
                Map.entry(Advancement.MEDIUM, List.of(
                        new Question(2, Advancement.MEDIUM, "abc", "text", null, List.of(
                                new Answer("tekst", true, "text"),
                                new Answer("tekst", false, "text"))))),
                Map.entry(Advancement.EXPERT, List.of(
                        new Question(3, Advancement.EXPERT, "abc", "text", null, List.of(
                                new Answer("tekst", true, "text"),
                                new Answer("tekst", false, "text"))))));

    }
    @Test
    void createExam(){
        // given
        IQuestionConnector iQuestionConnector = mock(IQuestionConnector.class);
        IQuestionService iQuestionService = new QuestionService(iQuestionConnector);
        JSONArray put = new JSONArray().put(new JSONObject()
                        .put("id", 1)
                        .put("advancement", "basic")
                        .put("category", "abc")
                        .put("text", "text")
                        .put("answers", new JSONArray()
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", true)
                                        .put("explanation", "text"))
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", false)
                                        .put("explanation", "text"))))
                .put(new JSONObject()
                        .put("id", 2)
                        .put("advancement", "medium")
                        .put("category", "abc")
                        .put("text", "text")
                        .put("answers", new JSONArray()
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", true)
                                        .put("explanation", "text"))
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", false)
                                        .put("explanation", "text"))))
                .put(new JSONObject()
                        .put("id", 3)
                        .put("advancement", "expert")
                        .put("category", "abc")
                        .put("text", "text")
                        .put("answers", new JSONArray()
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", true)
                                        .put("explanation", "text"))
                                .put(new JSONObject()
                                        .put("text", "tekst")
                                        .put("correct", false)
                                        .put("explanation", "text"))));
        when(iQuestionConnector.getJsonArray()).thenReturn(put);
        // when
        Exam exam = iQuestionService.createExam(1, 1,1, List.of("abc"));
        // then
        Exam exam1 = new Exam();
        exam1.getQuestionList().add(new Question(1, Advancement.BASIC, "abc", "text", null, List.of(
                new Answer("tekst", true, "text"),
                new Answer("tekst", false, "text"))));
        exam1.getQuestionList().add(new Question(2, Advancement.MEDIUM, "abc", "text", null, List.of(
                new Answer("tekst", true, "text"),
                new Answer("tekst", false, "text"))));
        exam1.getQuestionList().add(new Question(3, Advancement.EXPERT, "abc", "text", null, List.of(
                new Answer("tekst", true, "text"),
                new Answer("tekst", false, "text"))));
        assertThat(exam).isEqualTo(exam1);
    }
}
