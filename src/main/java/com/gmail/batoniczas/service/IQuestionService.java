package com.gmail.batoniczas.service;

import com.gmail.batoniczas.entity.Advancement;
import com.gmail.batoniczas.entity.Exam;
import com.gmail.batoniczas.entity.Question;

import java.util.List;
import java.util.Map;

public interface IQuestionService {
    /**
     * Creates List with {@link Question} from URL
     *
     * @return List with {@link Question}
     */
    List<Question> createListQuestion();

    /**
     * Groups questions by {@link Advancement}
     *
     * @return map, where key is {@link Advancement}, and value are {@link Question}
     */
    Map<Advancement, List<Question>> questionByAdvancement();

    /**
     * Adds {@link Question} to List Question in {@link Exam}
     * Sets amount of exam questions by {@link Advancement}
     * Allows to decide how many questions from advancement should be included in this exam
     *
     * @param countBasic  count of basic questions
     * @param countMedium count of medium questions
     * @param countExpert count of expert questions
     * @param categories  which categories should be included from: JAVA_LANGUAGE, GENERAL, JAVA_SOFTWARE, DESIGN_PATTERNS, SPRING
     * @return {@link Exam} with randomize questions
     */
    Exam createExam(int countBasic, int countMedium, int countExpert, List<String> categories);
}
