package com.gmail.batoniczas.service;

import com.gmail.batoniczas.entity.Answer;
import com.gmail.batoniczas.entity.Exam;
import com.gmail.batoniczas.entity.Question;
import com.gmail.batoniczas.entity.Result;

public interface IConsoleService {
    /**
     * Prints {@link Question} and {@link Answer},
     * Arranges the answers so that they are always in a different order
     * Checks the correct answer
     *
     * @param question choosed {@link Question}
     */
    void printQuestion(Question question, Runnable runnable);

    void printResult(Exam exam);
    Result createResult(Exam exam);
}
