package com.gmail.batoniczas.service;

import org.json.JSONArray;

public interface IQuestionConnector {
    JSONArray getJsonArray();
}
