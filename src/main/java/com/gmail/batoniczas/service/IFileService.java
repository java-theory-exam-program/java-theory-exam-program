package com.gmail.batoniczas.service;

import com.gmail.batoniczas.entity.Result;
public interface IFileService {
    /**
     * Allows saving the result.
     * It creates a new file with the data or overwrites an existing file.
     * @param result
     */
    void saveResult(Result result);
}
