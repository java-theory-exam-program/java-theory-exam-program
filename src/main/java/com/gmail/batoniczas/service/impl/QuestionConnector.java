package com.gmail.batoniczas.service.impl;

import com.gmail.batoniczas.service.IQuestionConnector;
import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class QuestionConnector implements IQuestionConnector {
    private final JSONArray jsonArray;

    /**
     * Connects with URL file, parses readed files to json array and creates an object
     *
     * @throws IOException
     */
    public QuestionConnector() throws IOException {
        URL url = new URL("https://public.andret.eu/questions.json");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        JSONTokener jsonTokener = new JSONTokener(connection.getInputStream());
        this.jsonArray = new JSONArray(jsonTokener);
    }

    @Override
    public JSONArray getJsonArray() {
        return jsonArray;
    }
}
