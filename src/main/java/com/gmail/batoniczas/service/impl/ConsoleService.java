package com.gmail.batoniczas.service.impl;

import com.gmail.batoniczas.entity.Answer;
import com.gmail.batoniczas.entity.Exam;
import com.gmail.batoniczas.entity.Question;
import com.gmail.batoniczas.entity.Result;
import com.gmail.batoniczas.service.IConsoleService;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Responsibiles for communicate with user, retrieves and validates the answers
 */
public class ConsoleService implements IConsoleService {
    private final Scanner scanner = new Scanner(System.in);

    @Override
    public void printQuestion(Question question, Runnable runnable) {
        List<Answer> answers = new ArrayList<>(question.answerList());
        Collections.shuffle(answers);
        System.out.println("Category: " + question.category());
        System.out.println("Advancement: " + question.advancement());
        System.out.printf("%d. %s%n", question.id(), question.text());
        Optional.of(question)
                .map(Question::code)
                .ifPresent(System.out::println);
        for (char i = 'a'; i < 'a' + answers.size(); i++) {
            System.out.println(i + ") " + answers.get(i - 'a').text());
        }
        char answer = readAnswer(answers.size());
        int userAnswerIndex = answer - 'a';
        Answer userAnswer = answers.get(userAnswerIndex);
        Answer correctAnswer = answers.stream()
                .filter(Answer::correct)
                .findAny()
                .orElse(null);
        System.out.printf("The correct answer is: %s %n", (char) (answers.lastIndexOf(correctAnswer) + 'a'));
        System.out.println(userAnswer.explanation());
        System.out.println();
        if (userAnswer.correct()) {
            runnable.run();
        }
    }

    private char readAnswer(int answers) {
        while (true) {
            char userAnswerLetter = scanner.nextLine().charAt(0);
            if (userAnswerLetter >= 'a' && userAnswerLetter <= 'a' + answers) {
                return userAnswerLetter;
            }
            System.out.println("You have to write letter of answer. ");
        }
    }

    @Override
    public void printResult(Exam exam) {
        System.out.printf("The maximum number of points to be earned is: %d%n", exam.maxScore());
        System.out.printf("The number of points you have earned is: %d%n", exam.getActualScore());
        System.out.printf("Your score in percentage: %.2f %% %n", exam.resultInProcent());
        System.out.printf("Has the exam been passed? %b%n", exam.isPassed());
    }


    @Override
    public Result createResult(Exam exam) {
        System.out.println("If you want to save your score, type your name: ");
        String answer = scanner.nextLine();
        if (answer.isEmpty()) {
            return null;
        }
        return new Result(answer, LocalDateTime.now(), exam.maxScore(), exam.getActualScore());
    }
}
