package com.gmail.batoniczas.service.impl;

import com.gmail.batoniczas.entity.Result;
import com.gmail.batoniczas.service.IFileService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileService implements IFileService {
    @Override
    public void saveResult(Result result) {
        Path path = Paths.get("result.json");
        try {
            JSONArray jsonArray = createJSONArray()
                    .put(createJSON(result));
            PrintWriter printWriter = new PrintWriter(path.toFile());
            printWriter.println(jsonArray.toString(4));
            printWriter.close();
        } catch (IOException e) {
            System.err.println("Problem with file. ");
        }
    }

    private JSONArray createJSONArray() {
        Path path = Paths.get("result.json");
        try {
            if (!Files.exists(path)) {
                return new JSONArray();
            }
            JSONTokener jsonTokener = new JSONTokener(new FileInputStream(path.toFile()));
            return new JSONArray(jsonTokener);
        } catch (IOException e) {
            System.err.println("Problem with file. ");
        }
        return new JSONArray();
    }

    private JSONObject createJSON(Result result) {
        return new JSONObject()
                .put("name", result.name())
                .put("date", result.date())
                .put("maxScore", result.maxScore())
                .put("score", result.score());
    }

}
