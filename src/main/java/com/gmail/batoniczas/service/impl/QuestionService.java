package com.gmail.batoniczas.service.impl;

import com.gmail.batoniczas.entity.Advancement;
import com.gmail.batoniczas.entity.Answer;
import com.gmail.batoniczas.entity.Exam;
import com.gmail.batoniczas.entity.Question;
import com.gmail.batoniczas.service.IQuestionConnector;
import com.gmail.batoniczas.service.IQuestionService;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Groups, draws and prepares list {@link Question} for the {@link Exam}
 */
public class QuestionService implements IQuestionService {
    private final IQuestionConnector connector;

    public QuestionService(IQuestionConnector connector) {
        this.connector = connector;
    }

    @Override
    public List<Question> createListQuestion() {
        JSONArray jsonArray = connector.getJsonArray();
        List<Question> questionList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            int id = jsonObject.getInt("id");
            Advancement advancement = Advancement.valueOf(jsonObject.getString("advancement").toUpperCase());
            String category = jsonObject.getString("category");
            String text = jsonObject.getString("text");
            String code = jsonObject.optString("code", null);

            List<Answer> answerList = new ArrayList<>();
            JSONArray jsonArrayAnswers = jsonObject.getJSONArray("answers");
            for (int j = 0; j < jsonArrayAnswers.length(); j++) {
                JSONObject jsonObjectAnswer = jsonArrayAnswers.getJSONObject(j);
                String textAnswer = jsonObjectAnswer.getString("text");
                boolean correct = jsonObjectAnswer.getBoolean("correct");
                String explanation = jsonObjectAnswer.getString("explanation");
                Answer answer = new Answer(textAnswer, correct, explanation);
                answerList.add(answer);
            }
            Question question = new Question(id, advancement, category, text, code, answerList);
            questionList.add(question);
        }
        Collections.shuffle(questionList);
        return questionList;
    }

    @Override
    public Map<Advancement, List<Question>> questionByAdvancement() {
        return createListQuestion().stream()
                .collect(Collectors.groupingBy(Question::advancement, Collectors.toList()));
    }

    @Override
    public Exam createExam(int countBasic, int countMedium, int countExpert, List<String> categories) {
        Exam exam = new Exam();

        questionByAdvancement().get(Advancement.BASIC).stream()
                .filter(x -> categories.contains(x.category()))
                .limit(countBasic)
                .forEach(x -> exam.getQuestionList().add(x));

        questionByAdvancement().get(Advancement.MEDIUM).stream()
                .filter(x -> categories.contains(x.category()))
                .limit(countMedium)
                .forEach(x -> exam.getQuestionList().add(x));

        questionByAdvancement().get(Advancement.EXPERT).stream()
                .filter(x -> categories.contains(x.category()))
                .limit(countExpert)
                .forEach(x -> exam.getQuestionList().add(x));
        return exam;
    }
}
