package com.gmail.batoniczas;

import com.gmail.batoniczas.entity.Exam;
import com.gmail.batoniczas.entity.Question;
import com.gmail.batoniczas.entity.Result;
import com.gmail.batoniczas.service.*;
import com.gmail.batoniczas.service.impl.ConsoleService;
import com.gmail.batoniczas.service.impl.FileService;
import com.gmail.batoniczas.service.impl.QuestionConnector;
import com.gmail.batoniczas.service.impl.QuestionService;
import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        IQuestionService questionService = new QuestionService(new QuestionConnector());
        IConsoleService consoleService = new ConsoleService();
        FileService fileService = new FileService();

        Exam exam = questionService.createExam(1, 1, 1, List.of("JAVA_LANGUAGE"));
        List<Question> questionList = exam.getQuestionList();
        for (Question question : questionList) {
            consoleService.printQuestion(question, () -> exam.addPoints(question.advancement().getMultiplier()));
        }
        consoleService.printResult(exam);
        Result result = consoleService.createResult(exam);
        if (result != null) {
            fileService.saveResult(result);
        }
    }
}
