package com.gmail.batoniczas.entity;

public enum Advancement {
    BASIC(1),
    MEDIUM(2),
    EXPERT(3);
    private final int multiplier;

    Advancement(int multiplier) {
        this.multiplier = multiplier;
    }

    public int getMultiplier() {
        return multiplier;
    }
}
