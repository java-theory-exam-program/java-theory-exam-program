package com.gmail.batoniczas.entity;

import java.util.List;

public record Question(int id, Advancement advancement, String category, String text, String code, List<Answer> answerList) {
}
