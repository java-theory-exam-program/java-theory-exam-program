package com.gmail.batoniczas.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Exam {
    private final List<Question> questionList = new ArrayList<>();
    private int actualScore;

    public List<Question> getQuestionList() {
        return questionList;
    }

    public int maxScore() {
        return questionList.stream()
                .map(Question::advancement)
                .mapToInt(Advancement::getMultiplier)
                .sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exam exam = (Exam) o;
        return actualScore == exam.actualScore && Objects.equals(questionList, exam.questionList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionList, actualScore);
    }

    public int getActualScore() {
        return actualScore;
    }

    public void addPoints(int point) {
        actualScore += point;
    }

    public double resultInProcent() {
        return 100. * actualScore / maxScore();
    }

    public boolean isPassed() {
        return resultInProcent() > 60.0;
    }

    @Override
    public String toString() {
        return "Exam{" +
                "questionList=" + questionList +
                ", actualScore=" + actualScore +
                '}';
    }
}
