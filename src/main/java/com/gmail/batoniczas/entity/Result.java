package com.gmail.batoniczas.entity;

import java.time.LocalDateTime;

public record Result (String name, LocalDateTime date, int maxScore, int score){

}
