package com.gmail.batoniczas.entity;

public record Answer(String text, boolean correct, String explanation) {
}
