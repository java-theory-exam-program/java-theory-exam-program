# JAVA QUIZ


#### Technology: Java, Spring, JSON, asynchronous queries

#### Functionalities:
- Executing asynchronous queries on the client side to the exposed API
- Selecting the number of questions from a given difficulty level and category from the list.
- Retrieving questions to the list.
- Mixing answers and assigning them to values: a, b, c, and d.
- Displaying the question along with its ID, difficulty level, and category, and displaying the answers.
- Upon user selection of the answer letter, an explanation of the issue appears.
- Questions follow one another, and in the end, points are counted.
- Displaying the score and information about whether the test has been passed.
- Depending on the level of advancement, questions have different point values.
- The user passes the exam when achieving a score above 60%.
- The user is asked whether they want to save their score.
- If yes, the user must provide their name.
- The program creates a file that saves the date, user's name, score, and the maximum possible number of points that can be obtained in the exam.
